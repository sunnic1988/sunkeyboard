﻿from ctypes import windll
from os.path import join, dirname
import time
from win32con import WM_INPUTLANGCHANGEREQUEST, VK_CAPITAL
import win32gui
import win32api
import win32con

# DD虚拟码，可以用DD内置函数转换。
vk = {'5': 205, 'c': 503, 'n': 506, 'z': 501, '3': 203, '1': 201, 'd': 403, '0': 210, 'l': 409, '8': 208, 'w': 302,
      'u': 307, '4': 204, 'e': 303, '[': 311, 'f': 404, 'y': 306, 'x': 502, 'g': 405, 'v': 504, 'r': 304, 'i': 308,
      'a': 401, 'm': 507, 'h': 406, '.': 509, ',': 508, ']': 312, '/': 510, '6': 206, '2': 202, 'b': 505, 'k': 408,
      '7': 207, 'q': 301, "'": 411, '\\': 313, 'j': 407, '`': 200, '9': 209, 'p': 310, 'o': 309, 't': 305, '-': 211,
      '=': 212, 's': 402, ';': 410}

# 需要组合shift的按键。
vk2 = {'"': "'", '#': '3', ')': '0', '^': '6', '?': '/', '>': '.', '<': ',', '+': '=', '*': '8', '&': '7', '{': '[',
       '_': '-',
       '|': '\\', '~': '`', ':': ';', '$': '4', '}': ']', '%': '5', '@': '2', '!': '1', '(': '9'}

# 语言代码
# https://msdn.microsoft.com/en-us/library/cc233982.aspx
LID = {0x0804: "Chinese (Simplified) (People's Republic of China)",
       0x0409: 'English (United States)'}

def getDll(bit):
    # 注册DD DLL，64位python用64位，32位用32位，具体看DD说明文件。
    # 测试用免安装版。
    # 用哪个就调用哪个的dll文件。
    dllPath = None
    if (bit == 32):
        dllPath = join(dirname(__file__),r'SunKeyboard.lib\DD96699.64.dll')
        # dllPath = r'D:\Program Files (x86)\UiBot Creator Community\user_ext\python\SunKeyboard.lib\DD94687.32.dll'
    if (bit == 64):
        dllPath = join(dirname(__file__),r'SunKeyboard.lib\DD96699.64.dll')
        # dllPath = r'D:\Program Files (x86)\UiBot Creator Community\user_ext\python\SunKeyboard.lib\DD94687.64.dll'
    print(dllPath)
    dd_dll = windll.LoadLibrary(dllPath)
    return dd_dll

def down_up(dd_dll, code):
    # 进行一组按键。
    dd_dll.DD_key(vk[code], 1)
    time.sleep(0.5)
    dd_dll.DD_key(vk[code], 2)

def dd(dd_dll, key):
    # 500是shift键码。
    if key.isupper():
        # 如果是一个大写的玩意。
        # 按下抬起。
        dd_dll.DD_key(500, 1)
        down_up(dd_dll, key.lower())
        dd_dll.DD_key(500, 2)

    elif key in '~!@#$%^&*()_+{}|:"<>?':
        # 如果是需要这样按键的玩意。
        dd_dll.DD_key(500, 1)
        down_up(dd_dll,vk2[key])
        dd_dll.DD_key(500, 2)
    else:
        down_up(dd_dll,key)

def driveLevelInput(text, bit):
    dd_dll = getDll(bit)
    st = dd_dll.DD_btn(9884625)
    print(st)
    #等待2秒
    time.sleep(2)
    for i in text:
        dd(dd_dll, i)
    dd_dll = None

# 切换成英文输入法
def changeToEnglishInput(language):
    # 获取前景窗口句柄
    hwnd = win32gui.GetForegroundWindow()
    # 获取前景窗口标题
    title = win32gui.GetWindowText(hwnd)
    print('当前窗口：' + title)
    # 获取键盘布局列表
    im_list = win32api.GetKeyboardLayoutList()
    im_list = list(map(hex, im_list))
    print(im_list)
    if (language == "EN"):
        # 设置键盘布局为英文
        result = win32api.SendMessage(hwnd,WM_INPUTLANGCHANGEREQUEST,0,0x0409)
        if result == 0:
            return '设置英文键盘成功！'
    elif (language == "ZH"):
        # 设置键盘布局为中文
        result = win32api.SendMessage(hwnd,WM_INPUTLANGCHANGEREQUEST,0,0x0804)
        if result == 0:
            return '设置中文键盘成功！'

# 大小写切换
def openCapslock(isCapital):
    # 0 为小写Capital，1位大写Lowercase
    bOn = win32api.GetKeyState(VK_CAPITAL)
    print("bOn: ", bOn)
    if (isCapital):
        if (bOn == 0):
            win32api.keybd_event(VK_CAPITAL, win32api.MapVirtualKey(VK_CAPITAL,0),0,0)
            time.sleep(0.02)
            win32api.keybd_event(VK_CAPITAL, win32api.MapVirtualKey(VK_CAPITAL,0), win32con.KEYEVENTF_KEYUP,0)
        return "设置大写状态成功"
    else:
        if (bOn == 1):
            win32api.keybd_event(VK_CAPITAL, win32api.MapVirtualKey(VK_CAPITAL, 0), 0, 0)
            time.sleep(0.02)
            win32api.keybd_event(VK_CAPITAL, win32api.MapVirtualKey(VK_CAPITAL, 0), win32con.KEYEVENTF_KEYUP, 0)
        return "设置小写状态成功"

if __name__ == '__main__':
    changeToEnglishInput("EN")
    driveLevelInput("Password1",64)
    # changeToEnglishInput("ZH")
    # openCapslock(True)
    # openCapslock(False)